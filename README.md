# Ansible PlexPy
Install and configure PlexPy on CentOS/RHEL or Ubuntu/Debian.

### PlexPy configuration
```
plexpy_dir: /var/lib/plexpy 									# plexpy install dir
plexpy_user: plexpy 											# plexpy user
plexpy_group: plexpy 											# plexpy group
plexpy_repo: https://github.com/drzoidberg33/plexpy.git 		# plexpy repo
```

## Dependencies
None.

## Example Playbook
    - hosts: whatever
      become: yes
      roles:
        - plex